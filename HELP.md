Customer portal is the applicaiton which is used to onboard users into the system.

This applicaiton rest endpoint throuth we can onboard a user. For security we used Self-signed SSL
certificate, for token security we used JWT token and for Man in middle attach we used 2 step authentication
using email channel and SSL.

This applicaiton contains the third party api(Mailjet) for sending the email notificaiton to user. 
So for run this application first we need to follow below step: 
 
 1) Create a account in Mailjet(https://app.mailjet.com/signup) 
 2) Get the api key and api secret from the mailjet account and run the below command on the console
    export MJ_APIKEY_PUBLIC="your API key"
    export MJ_APIKEY_PRIVATE="your API secret"
 3) After that go to the application.properties and update "mailjet.sender.email" with the email configured in the mailjet account for sending the email.
 4) Go to project dir and run the command "mvn clean package"
 5) Run "java -jar target/customer-0.0.1-SNAPSHOT.jar" to run the application
 
 
 Application contains some rest end point. I am adding the postman collection for calling the api in resource.postman dir
 
 
 